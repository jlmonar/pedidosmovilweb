@extends('layouts.app')

@section('title', 'Factura')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Datos de facturación</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('pedido.store') }}" onsubmit="disableButton()">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('identificacion') ? ' has-error' : '' }}">
                                <label for="identificacion" class="col-md-4 control-label">Identificacion</label>

                                <div class="col-md-6">
                                    <!--
                                    <p >{{$cliente->CODIGO}}</p>
                                    -->
                                    <input id="identificacion" type="text" class="form-control" name="identificacion" value="{{$cliente->CODIGO}}" required autofocus>

                                    @if ($errors->has('identificacion'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('identificacion') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Nombres</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{$cliente->NOMBRE}}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                                <label for="apellido" class="col-md-4 control-label">Apellidos</label>

                                <div class="col-md-6">
                                    <input id="apellido" type="text" class="form-control" name="apellido" value="{{$cliente->APELLIDO}}" required autofocus>

                                    @if ($errors->has('apellido'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('apellido') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Correo Electrónico</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{$cliente->MAIL}}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('direccion') ? ' has-error' : '' }}">
                                <label for="direccion" class="col-md-4 control-label">Dirección de Domicilio</label>

                                <div class="col-md-6">
                                    <input id="direccion" type="text" class="form-control" name="direccion" value="{{$cliente->DOMICILIO}}" required autofocus>

                                    @if ($errors->has('direccion'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('direccion') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
                                <label for="telefono" class="col-md-4 control-label">Teléfono</label>

                                <div class="col-md-6">
                                    <input id="telefono" type="number" class="form-control" name="telefono" value="{{$cliente->CL_CELULAR}}" required autofocus>

                                    @if ($errors->has('telefono'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('telefono') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-check form-check-inline col-md-offset-4 col-md-4 ">
                                    <input class="form-check-input" type="radio" name="opcion_entrega" id="inlineRadio1" value="2" checked="checked">
                                    <label class="form-check-label" for="inlineRadio1">Entrega a domicilio</label>
                                </div>
                                <div class="form-check form-check-inline col-md-4">
                                    <input class="form-check-input" type="radio" name="opcion_entrega" id="inlineRadio2" value="1">
                                    <label class="form-check-label" for="inlineRadio2">Para retirar</label>
                                </div>
                            </div>

                            <hr style="border-top: 4px double #cecece;">

                            <div class="form-group" style="display: flex;">
                                    <div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-4">
                                        <p>SUBTOTAL:</p>
                                    </div>
                                    <div class="col-md-6 col-sm-3 col-xs-3">
                                        <p>{{ round(Cart::getSubtotal(), 2) }} $</p>
                                    </div>
                            </div>

                            <div class="form-group"  style="display: flex;">
                                <div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-4">
                                    <p>IVA 12%:</p>
                                </div>
                                <div class="col-md-6 col-sm-3 col-xs-3">
                                    <p>{{ round(Cart::getSubtotal() *0.12, 2)}} $</p>
                                </div>
                            </div>

                            <div class="form-group"  style="display: flex;">
                                <div class="col-md-2 col-sm-2  col-xs-3 col-md-offset-4">
                                    <p>TOTAL:</p>
                                </div>
                                <div class="col-md-6 col-sm-3 col-xs-3">
                                    <p>{{ round(Cart::getTotal() * 1.12, 2) }} $</p>
                                </div>
                            </div>

                            <div class="form-group"  style="display: flex;">
                                <div class="col-md-2 col-md-offset-4">
                                    <a href="{{route('cart.index')}}" class="btn btn-danger">
                                        VOLVER
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <input type="submit" id="realizar-pedido" class="btn btn-danger" value="REALIZAR PEDIDO"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function disableButton() {
            $("#realizar-pedido").attr("disabled", true);
            $("#realizar-pedido").val("REGISTRANDO PEDIDO...")
        }
    </script>
@endsection
