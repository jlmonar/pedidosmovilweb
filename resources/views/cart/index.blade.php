@extends('layouts.app')

@section('title', 'Carrito de compras')

@section('content')
    <div class="container" style="margin-bottom: 22px">
        {{--
        <h1>Tu pedido</h1>
        --}}
        <hr>

        @if (!Cart::isEmpty())
            <div class="row" style="margin-bottom: 20px">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <a href="{{ route('lineas.index') }}" class="btn btn-primary btn-lg">Continuar comprando</a>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-3">
                    <!--
                    <a href="{{route('factura')}}" class="btn btn-success btn-lg">Realizar pedido</a>
                    -->
                    <button class="btn btn-success btn-lg" id="pedido-factura">Realizar pedido</button>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-6">
                    <form action="{{ route('clear-cart') }}" method="GET">
                        {!! csrf_field() !!}
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="submit" class="btn btn-danger btn-lg pull-right" onclick="return confirm('¿Seguro que desea vaciar carrito de pedido?')">
                            <svg class="bi bi-trash2-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path d="M2.037 3.225l1.684 10.104A2 2 0 0 0 5.694 15h4.612a2 2 0 0 0 1.973-1.671l1.684-10.104C13.627 4.224 11.085 5 8 5c-3.086 0-5.627-.776-5.963-1.775z"/>
                                <path fill-rule="evenodd" d="M12.9 3c-.18-.14-.497-.307-.974-.466C10.967 2.214 9.58 2 8 2s-2.968.215-3.926.534c-.477.16-.795.327-.975.466.18.14.498.307.975.466C5.032 3.786 6.42 4 8 4s2.967-.215 3.926-.534c.477-.16.795-.327.975-.466zM8 5c3.314 0 6-.895 6-2s-2.686-2-6-2-6 .895-6 2 2.686 2 6 2z"/>
                            </svg>
                        </button>
                    </form>
                </div>
            </div>

            <h4>Dirección de entrega</h4>
            <div class="row" style="margin-bottom: 10px">
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <p id="direccion-entrega">No seleccionado</p>
                </div>

                <div class="col-xs-6 col-sm-4 col-md-3">
                    <a href="{{ url('/map') }}" class="btn btn-danger">CAMBIAR</a>
                </div>

            </div>

            <div class="panel panel-default" >
                <div class="panel-heading" style="font-weight: bold; font-size: 18px;">
                    Tu pedido
                    <!--
                    <a class="pull-right" href="{{route('pdf.index')}}" target="_blank" title="Descargar PDF" style="margin-right: 10px;">
                        <i class="fa fa-download" aria-hidden="true"></i>
                    </a>
                    -->
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                        <th class="column-spacer"></th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($data as $item)
                        <tr>
                            <td width="35%"><a href="{{ route('productos.show', $item['id']) }}">{{ $item['name'] }}</a></td>
                            <td width="45%">
                                <div class="col-xs-6 col-sm-6 col-md-6" style="padding-left: 0px; padding-right: 0px">
                                    {!!Form::number('cantidad', $item['quantity'], ['class' => 'form-control', 'min' => '0','required', 'id' => 'qty-'.$item['id']]) !!}
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6" style="padding-left: 0px; padding-right: 0px">
                                    <button type="submit" class="btn btn-primary btn-edit" data-id="{{$item['id']}}">
                                        <i class="fa fa-refresh" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </td>
                            <td width="15%">$ {{$item['price'] * $item['quantity']}}</td>
                            <td width="5%">
                                {!! Form::open(['route' =>  ['cart.destroy', $item['id']] , 'method' => 'DELETE']) !!}
                                {!! csrf_field() !!}
                                <button type="submit" class="btn btn-danger btn-sm" value="Eliminar" onclick="return confirm('¿Seguro que desea eliminarlo?')">
                                    <svg class="bi bi-trash-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
                                    </svg>
                                </button>
                                {!!Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td class="table-image"></td>
                        <td></td>
                        <td class="small-caps table-bg" style="text-align: right">Subtotal</td>
                        <td>$ {{ Cart::getSubtotal() }}</td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr class="border-bottom">
                        <td class="table-image"></td>
                        <td style="padding: 40px;"></td>
                        <td class="small-caps table-bg" style="text-align: right">Total a pagar</td>
                        <td class="table-bg">$ {{ Cart::getTotal() }}</td>
                        <td class="column-spacer"></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        @else
            <h3>No tienes items en tu carrito de compras</h3>
            <a href="{{ route('lineas.index') }}" class="btn btn-primary btn-lg">Continuar comprando</a>
        @endif
    </div>

    <script>
        function initMap() {
            @if(Session::has('latitud') && Session::has('longitud'))
                var latitud = "{{Session::get('latitud')}}";
                var longitud = "{{Session::get('longitud')}}";

                var direccion_entrega =  document.getElementById('direccion-entrega');

                var geocoder = new google.maps.Geocoder();

                var latlng = new google.maps.LatLng(latitud,longitud);
                geocoder.geocode({
                    'latLng': latlng
                }, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            direccion_entrega.innerHTML = results[0].formatted_address;
                            console.log(results[0].formatted_address);
                        } else {
                            alert('No results found');
                        }
                    } else {
                        alert('Geocoder failed due to: ' + status);
                    }
                });
            @endif
        }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key={{env('GOOGLE_API_KEY')}}&callback=initMap"></script>
@endsection

@section('extra-js')
    <script>
        $(document).ready(function(){
            $("a.submit").click(function(){
                var id = $(this).attr('id');
                console.log(id);
                if (confirm('¿Seguro que desea eliminarlo?')) {
                    document.getElementById("form-"+id).submit();
                }

            });
        });

        (function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(".btn-edit").click(function(e) {
                var id = $(this).attr('data-id');
                var qty = $('#qty-' + id).val();

                $.ajax({
                    type: "PATCH",
                    url: '{{ url("/cart") }}' + '/' + id,
                    data: {
                        'quantity': qty,
                    },
                    success: function(data) {
                        window.location.href = '{{ url('/cart') }}';
                    }
                });
            });
        })();

        $("#pedido-factura").click( function() {
            @if(Session::has('latitud') && Session::has('longitud'))
                 var lat = {{Session::get('latitud')}}
                 var lon = {{Session::get('longitud')}}
                 console.log(lat);
                 console.log(lon);
                 window.location.href = "{{route('factura')}}";
            @else
                console.log('NO HAY COORDENADAS');
                toastr.warning("Elija dirección de destino antes de continuar. Presione el botón CAMBIAR para elegir su dirección de entrega");
            @endif
        });
    </script>
@endsection