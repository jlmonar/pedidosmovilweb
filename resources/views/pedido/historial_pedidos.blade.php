
@extends('layouts.app')

@section('title', 'Historial de pedidos')

@section('content')
    {{--
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    HISTORIAL DE PEDIDOS
    <p>Date: <input type="text" id="datepicker" class="form-control"></p>

    <script type="text/javascript">
        $( "#datepicker" ).datepicker();
    </script>
    --}}

    <!-- Fuente: http://www.daterangepicker.com/ -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    {{--
    <input type="text" name="datetimes"  class="form-control"/>
    --}}

    <div class="container">
        <div class="form-group">
            <div class="row">
                {!! Form::model(Request::all(), ['route' => 'consultar-pedidos', 'method' => 'GET']) !!}
                <div class="col-xs-12 col-sm-4 col-md-4">
                    {!!Form::label('fecha_inicio_fin', 'Fecha Inicio:', ['class' => 'control-label'])!!}
                    {!!Form::text('fecha_inicio', null, ['class' => 'form-control', 'placeholder' => 'Fecha Inicio...', 'aria-descridbedby' => 'search', 'required' => true]) !!}
                    <br>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    {!!Form::label('fecha_inicio_fin', 'Fecha Fin:', ['class' => 'control-label'])!!}
                    {!!Form::text('fecha_fin', null, ['class' => 'form-control', 'placeholder' => 'Fecha Fin...', 'aria-descridbedby' => 'search', 'required' => true]) !!}
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <br>
                    <button type="submit" class="btn btn-default btn-search-product">Buscar</button>
                </div>
                {!!Form::close() !!}
            </div>
        </div>

        @if(isset($pedidos))
        <table id="table-productos" class="table table-bordered">
            <thead>
            <th>Secuencia</th>
            <th>Fecha</th>
            <th>Valor total</th>
            <th>Acción</th>
            </thead>

            <tbody>
            @foreach($pedidos as $key=>$pedido)
                @if(($key%2) == 0)
                    <tr style="background-color: #e8e8e8;" id="tr-{{$pedido->CINV_SEC}}">
                @else
                    <tr id="tr-{{$pedido->CINV_SEC}}">
                @endif
                        <td>{{$pedido->CINV_SEC}}</td>
                        <td>{{$pedido->CINV_FECING}}</td>
                        <td>{{round($pedido->tbdinvs()->sum('DINV_VTA'), 2)}}</td>
                        <td style="text-align: center;">
                            <a class="btn btn-warning" href="{{ route('detalle_pedido.show', $pedido->CINV_SEC) }}">Visualizar</a>
                        </td>
                    </tr>
            @endforeach
            </tbody>

        </table>
        @endif

        @if(isset($pedidos))
            <div class="text-center">
                {{ $pedidos->appends(Request::all())->links() }}
            </div>
        @endif
    </div>


    <script>
        /*
        $('input[name="datetimes"]').daterangepicker({
            timePicker: true,
            startDate: moment().startOf('hour'),
            endDate: moment().startOf('hour').add(32, 'hour'),
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
        */

        $('input[name="fecha_inicio"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });


        $('input[name="fecha_fin"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });

        //$('input[name="fecha_inicio"]').val('');
        //$('input[name="fecha_fin"]').val('');
    </script>

@endsection