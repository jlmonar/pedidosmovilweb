
@extends('layouts.app')

@section('title', 'Detalle Pedido')

@section('content')
    <div class="container">
        <div class="row">
            <!-- Title -->
            <ol class="breadcrumb">
                <li><a href="{{ route('historial-pedidos') }}">Historial de pedidos</a></li>
                @if(isset($pedido))
                    <li class="active">Pedido # {{$pedido->CINV_SEC}}</li>
                @endif
            </ol>
        </div>

        <div class="row">
            {!!Form::label('fecha', 'Fecha:', ['class' => 'control-label col-xs-2 col-sm-1 col-md-1'])!!}
            <p class="col-xs-3 col-sm-3 col-md-3"> {{$pedido->CINV_FECING}}</p>
        </div>
        <div class="row">
            {!!Form::label('total', 'Total:', ['class' => 'control-label  col-xs-2 col-sm-1 col-md-1'])!!}
            <p class="col-xs-3 col-sm-34 col-md-3"> {{round($pedido->tbdinvs()->sum('DINV_VTA'), 2)}}</p>
        </div>

        <hr>

        @if(isset($detalles_pedido))
            <table id="table-productos" class="table table-bordered">
                <thead>
                <th>Linea</th>
                <th>Descripcion</th>
                <th>Cantidad</th>
                <th>Precio Unitario</th>
                <th>Valor total</th>
                </thead>

                <tbody>
                    @foreach($detalles_pedido as $key=>$detalle_pedido)
                        @if(($key%2) == 0)
                            <tr style="background-color: #e8e8e8;" id="tr-{{$detalle_pedido->DINV_LINEA}}">
                        @else
                            <tr id="tr-{{$detalle_pedido->DINV_LINEA}}">
                        @endif
                                <td>{{$detalle_pedido->DINV_LINEA}}</td>
                                <td>{{$detalle_pedido->producto->DESCRIPCION}}</td>
                                <td>{{$detalle_pedido->DINV_CANT}}</td>
                                <td>{{round($detalle_pedido->STOCK1, 2)}}</td>
                                <td>{{round($detalle_pedido->DINV_VTA, 2)}}</td>
                            </tr>
                     @endforeach
                </tbody>

            </table>
        @endif
    </div>
@endsection

