<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
<!--
    <title>{{ config('app.name', 'Laravel') }}</title>
-->
    <title>
        @yield('title')
    </title>
    <!-- Scripts -->
    {!!Html::script('https://code.jquery.com/jquery-2.2.4.js')!!}
    <!-- Bootstrap -->
    {!!Html::script('js/bootstrap.js')!!}

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/productos.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
</head>
<body>

    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ route('productos.index') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>

                    @if (!Auth::guest())
                        <a href="{{route('cart.index')}}">
                            <i class="fa fa-shopping-cart cart-icon-i" aria-hidden="true"></i>

                            <span class="badge" id="span-cart">
                                {{Cart::getTotalQuantity() == 0 ? '0' : (Cart::getTotalQuantity() >99 ? '99+' : Cart::getTotalQuantity())}}
                            </span>
                        </a>
                    @endif
                </div>

                <div class="collapse navbar-collapse " id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    @if (!Auth::guest())
                        <ul class="nav navbar-nav navbar-left">
                            <li class="nav-item {{{ ((Route::currentRouteNamed('lineas.index') || Route::currentRouteNamed('lineas-categoria-productos')) ? 'active' : '') }}}"><a href="{{route('lineas.index')}}">Categorias</a></li>
                            <li  class="nav-item {{{ (Route::currentRouteNamed('productos.index') ? 'active' : '') }}}"><a href="{{route('productos.index')}}">Productos</a></li>
                            <li  class="nav-item {{{ ((Route::currentRouteNamed('historial-pedidos') || Route::currentRouteNamed('consultar-pedidos')  || Route::currentRouteNamed('detalle_pedido.show')) ? 'active' : '') }}}"><a href="{{ route('historial-pedidos') }}">Historial Pedidos</a></li>
                            <li  class="nav-item {{{ ((Route::currentRouteNamed('perfil.index')) ? 'active' : '') }}}"><a href="{{ route('perfil.index') }}">Perfil Usuario</a></li>
                        </ul>
                    @endif

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Iniciar sesión</a></li>
                        @else
                            <!--
                            <li>
                                <a href="{{route('cart.index')}}">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>

                                    @if(Cart::getTotalQuantity())
                                        <span class="badge">
                                            {{Cart::getTotalQuantity() == 0 ? '' : (Cart::getTotalQuantity() >99 ? '99+' : Cart::getTotalQuantity())}}
                                        </span>
                                    @endif

                                </a>
                            </li>
                            -->

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->NOMBRE}} {{Auth::user()->APELLIDO}} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('historial-pedidos') }}">Historial de pedidos</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Cerrar Sesión
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @include('template.partials.flash-message')
        @include('template.partials.errors')
        @yield('content')
    </div>

    <!-- Scripts -->
    {{--
    <script src="{{ asset('js/app.js') }}"></script>
    --}}
    @yield('extra-js')
</body>
</html>
