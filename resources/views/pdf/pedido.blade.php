<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Su pedido</title>
    <!-- Styles -->
    <link href="{{ asset('css/pdf.css') }}" rel="stylesheet">
</head>
<body>

<main>
    <h1 class="pedido-titulo">PEDIDO #{{ $invoice }}</h1>
    <div id="details" class="clearfix">
        <div id="invoice">
            <h2 class="info">Fecha de pedido: {{ $date }}</h2>
            <h2 class="info">Cliente: Jose</h2>
            <h2 class="info">Dirección: Guayaquil</h2>
            <h2 class="info">Telefono: 09852546663</h2>
        </div>
    </div>
    <table border="0" cellspacing="0" cellpadding="0">
        <thead>
        <tr >
            <!--
            <th class="no">CANTIDAD</th>
            -->
            <th class="total cabecera" >DESCRIPCION</th>
            <th class="total cabecera">CANTIDAD</th>
            <th class="total cabecera">PRECIO UNITARIO</th>
            <th class="total cabecera">TOTAL</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($data as $item)
                <tr>
                    <!--
                    <td class="no">{{ $item['quantity'] }}</td>
                    -->
                    <td class="desc" >{{ $item['name'] }}</td>
                        <td class="desc" style="text-align: center">{{ $item['quantity'] }}</td>
                    <td class="desc" style="text-align: center">{{ $item['price'] }}</td>
                    <td class="desc" style="text-align: center">{{ $item['quantity'] * $item['price'] }} </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2"></td>
            <td style="font-weight:bold">TOTAL</td>
            <td style="font-weight:bold; text-align:center">$ {{ Cart::getTotal() }}</td>
        </tr>
        </tfoot>
    </table>
    </main>
</body>
</html>