@extends('layouts.app')

@section('title', 'Lista de productos')

@section('content')
    <div class="container">
        <div class="row categoria-div">
            <!-- Title -->
            <ol class="breadcrumb">
                <li><a href="{{ route("lineas.index") }}" class="fa fa-arrow-left"></a></li>

                @if(isset($nombre_linea))
                <li class="active">{{$nombre_linea->NOMBRE}}</li>
                @endif
            </ol>
        </div>


        <img class="img-categoria" src="http://corporacionsmartest.com/pedidos_app/restaurantes/{{$linea}}/restaurant.jpg">

        <div class="row" >
            @if(isset($categorias))
                <ul class="list-group" id="categorias-linea">
                    @if($subcategoria == "all")
                        <li class="list-group-item active" id="all">
                    @else
                        <li class="list-group-item"  id="all">
                            @endif
                            Todos</li>

                        @foreach($categorias as $categoria)
                            @if($subcategoria == $categoria->CODIGO)
                                <li class="list-group-item active" id="{{$categoria->CODIGO}}">
                            @else
                                <li class="list-group-item" id="{{$categoria->CODIGO}}">
                                    @endif
                                    {{ $categoria->NOMBRE }}</li>
                                @endforeach
                </ul>
            @endif
        </div>

        <items linea="{{$linea}}" categoria="{{$subcategoria}}" nivel-precio-cliente="{{$nivel_precio_cliente}}" site-url="{{env('SITE_URL')}}"></items>

    </div>
@endsection

@section('extra-js')
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#categorias-linea li").click(function(e) {
                var linea = {!! json_encode($linea) !!};
                var categoria = e.target.id;

                window.location = linea + '?categoria=' + categoria;
            });
        });
    </script>
@endsection