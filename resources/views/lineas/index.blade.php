@extends('layouts.app')

@section('title', 'Lista de Lineas')

@section('content')
    <div class="containerr">
        <div id="panel-productos" class="panel panel-default">
            <div class="panel-heading clearfix">
                <h4 id="panel-title-productos" class="panel-title pull-left">Lista de Lineas</h4>
                {{-- Buscador de productos --}}

                {!! Form::model(Request::all(), ['route' => 'lineas.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
                <div class="form-group">
                    {!! Form::text('nombre', null, ['class' => 'form-control search-product', 'placeholder' => 'Nombre Linea...', 'aria-descridbedby' => 'search', 'required' => true]) !!}
                </div>
                <button type="submit" class="btn btn-default btn-search-product">Buscar</button>

                {!!Form::close() !!}
            </div>
            <div class="panel-body">
                <lineas texto-busqueda="{{$texto_busqueda}}" site-url="{{env('SITE_URL')}}"></lineas>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
    <script src="{{ asset('js/app.js') }}"></script>
@endsection