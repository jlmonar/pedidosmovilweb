<!DOCTYPE html>
<html lang="en">
    <body>
    Su pedido ha sido realizado con éxito, y se encuentra disponible para descargar.
    <br> <br>
    Saludos cordiales,
    <br>
    {{env('NOMBRE_EMPRESA')}}
    </body>
</html>