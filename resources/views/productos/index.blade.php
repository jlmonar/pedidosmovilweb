@extends('layouts.app')

@section('title', 'Lista de productos')

@section('content')
    <div class="containerf">

        <div class="row">
            <div class="col-md-12" >
                <div id="panel-productos" class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <h4 id="panel-title-productos" class="panel-title pull-left">Lista de productos</h4>
                        {{-- Buscador de productos --}}

                        {!! Form::model(Request::all(), ['route' => 'productos.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
                        <div class="form-group">
                            {!! Form::text('nombre', null, ['class' => 'form-control search-product', 'placeholder' => 'Nombre Producto...', 'aria-descridbedby' => 'search', 'required' => true]) !!}
                        </div>
                        <button type="submit" class="btn btn-default btn-search-product">Buscar</button>

                        {!!Form::close() !!}
                    </div>
                    <div class="panel-body">
                        <div id="app">
                            <items texto-busqueda="{{$texto_busqueda}}" nivel-precio-cliente="{{$nivel_precio_cliente}}" site-url="{{env('SITE_URL')}}"></items>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#agregar-masivo").click(function() {
                /*
                * http://manohark.com/read-td-values-inside-select-input-tag-using-jquery/
                */
                var TableData = [];
                var i=0;

                $('#table-productos > tbody > tr').each(function(row, tr) {

                    var trId = this.id; // button ID
                    var arrayId = trId.split('-');
                    var cantidad = $('td:eq(4) input',this).val();

                    if (cantidad != '' && cantidad >0) {
                        TableData[i]={
                            "numeroItem": arrayId[1],
                            "cantidad": $('td:eq(4) input',this).val()
                        };
                        i++;
                    }
                });
                if ( i > 0) {
                    var outputValue=JSON.stringify(TableData);

                    window.location.replace("addItemsGroup/" + outputValue);
                } else {
                    alert('No ha ingresado la cantidad de ningún producto.');
                }
            });
        });
    </script>
@endsection

@section('extra-js')
    <script src="{{ asset('js/app.js') }}"></script>
@endsection