@extends('layouts.app')

@section('title', 'Detalle Producto')

@section('content')
    <div id='infoProducto' class="container" style="margin-bottom: 22px">
        <div class="row">
            <!-- Title -->
            <ol class="breadcrumb breadcrumb-producto-detail">
                <li><a href="javascript:history.back()" class="fa fa-arrow-left"></a></li>
                @if(isset($producto))
                    <li class="active">{{$producto->DESCRIPCION}}</li>
                @endif
            </ol>
        </div>
        <div class="row">
            <!-- Product Info-->
            @if(isset($producto))
                @php
                    $url= url()->previous();
                @endphp
                <div class="col-xs-12 col-sm-6">
                    <img src="http://corporacionsmartest.com/pedidos_app/platos_comida/{{$producto->NUMERO_ITEM}}/food.jpg" onerror="this.onerror=null; this.src='../images/no_disponible.png'" alt="Producto" class="img-responsive img_product">
                </div>
                <div class="col-xs-12 col-sm-6 producto-detalles">
                    <h2 id="product-title">{{$producto->DESCRIPCION}} </h2>

                    <p class="product_prize">$ {{number_format($producto->PRECIOD_VTA_1, 2)}}</p>
                    <br>

                    <div class="row bloque-add-minus">
                        <button id="btn-minus-quantity" disabled='true'  type="button" class="btn btn-danger  btn-circle btn-xl col-xs-5 col-sm-5 col-md-5"><i class="fa fa-minus"></i></button>
                        <p id="cantidad-producto" class="col-xs-2 col-sm-2 col-md-2 product_cantidad">1</p>
                        <button id="btn-plus-quantity" type="button" class="btn btn-danger btn-circle btn-xl col-xs-5  col-sm-5 col-md-5"><i class="fa fa-plus"></i></button>
                    </div>

                    <br>

                    <button id="agregar-producto" class="btn btn-danger add-cart" style="width: 200px;">Agregar</button>

                </div>
            @endif
        </div>
    </div>

    <script type="text/javascript">
        $("#btn-plus-quantity").click(function (e) {
            var cantidad = Number($("#cantidad-producto").html());
            cantidad = cantidad + 1;

            $('#cantidad-producto').text(cantidad);

            if (cantidad > 1) {
                $("#btn-minus-quantity").prop('disabled', false);
            }
        });

        $("#btn-minus-quantity").click(function (e) {
            var cantidad = Number($("#cantidad-producto").html());
            cantidad = cantidad - 1;

            if (cantidad > 0 ) {
                $('#cantidad-producto').text(cantidad);
            }
            if (cantidad == 1) {
                $("#btn-minus-quantity").prop('disabled', true);
            }
        });
    </script>

    <script>
        (function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#agregar-producto").click(function(e) {
                var producto = {!! json_encode($producto) !!};
                var id = producto['NUMERO_ITEM'];
                var cantidad = Number($("#cantidad-producto").html());

                $("#agregar-producto").attr("disabled", true);

                $.ajax({
                    type: "GET",
                    url: '{{ url("/addItem") }}' + '/' + id,
                    data: {
                        'cantidad': cantidad
                    },
                    success: function(data) {
                        if (data['success']) {
                            var cantidad = data['cantidad'];

                            if (cantidad > 99) {
                                cantidad = '99+'
                            }

                            $("#span-cart").html(cantidad);

                            window.location.href="{{url($url)}}";
                            toastr.success("Producto agregado al carrito de compras.");
                        } else {
                            toastr.error("Error al agregar producto al carrito de compras. Contacte a soporte.");
                            $("#agregar-producto").attr("disabled", false);
                        }
                    }
                });
            });
        })();

        @if(Session::has('message'))
        var type="{{Session::get('alert-type','info')}}"

        switch(type){
            case 'info':
                toastr.info("{{ Session::get('message') }}");
                break;
            case 'success':
                toastr.success("{{ Session::get('message') }}");
                break;
            case 'warning':
                toastr.warning("{{ Session::get('message') }}");
                break;
            case 'error':
                toastr.error("{{ Session::get('message') }}");
                break;
        }
        @endif
    </script>
@endsection