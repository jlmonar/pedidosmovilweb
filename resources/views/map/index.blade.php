@extends('layouts.app')

@section('title', 'Map')

@section('content')
    <style>
        #contenedor_map {
            height: 352px;
            width: 352px;
            border: 1px solid;
            margin: auto;
        }
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 350px;
            width: 350px;
            /*margin: auto;*/
        }
    </style>
    <h4 style="text-align: center">Seleccione dirección de entrega</h4>
    <div id="contenedor_map">

        <!-- Search input -->
        <input id="searchInput" class="controls form-control" type="text" placeholder="¿Dónde te encuentras?">

        <!-- Google map -->
        <div id="map"></div>

        <!-- MARKER CENTER -->
        <div id="static-img">
            <img src="http://mt.googleapis.com/vt/icon/name=icons/spotlight/spotlight-poi.png">
        </div>
    </div>

    <hr>

    {!! Form::open(['route' =>  'map-direction', 'method' => 'POST']) !!}
    {!! csrf_field() !!}
        <input type="hidden" id="lat-ubicacion" name="latitud">
        <input type="hidden" id="lng-ubicacion" name="longitud">
        <input type="submit" id="seleccionar-ubicacion" class="btn btn-danger" value="SELECCIONAR UBICACIÓN">
    {!!Form::close() !!}

    <script>
        var map;

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -2.131054, lng: -79.892042},
                zoom: 16,
                fullscreenControl: false,
                mapTypeControl: false
            });

            ///////////////////////////////////////
            var centerMarker = document.getElementById('static-img');
            map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerMarker);

            var input = document.getElementById('searchInput');
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("Autocomplete's returned place contains no geometry");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }


                console.log('lat: ' + place.geometry.location.lat());
                console.log('lng: ' + place.geometry.location.lng());
            });


            ///////////////////////////////////////

            // Create the DIV to hold the control and call the constructor passing in this DIV
            var geolocationDiv = document.createElement('div');
            var geolocationControl = new GeolocationControl(geolocationDiv, map);

            map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(geolocationDiv);
        }

        function GeolocationControl(controlDiv, map) {

            // Set CSS for the control button
            var controlUI = document.createElement('div');
            controlUI.style.height = '40px';
            controlUI.style.width = '40px';
            controlUI.style.marginBottom = '15px';
            controlUI.style.marginRight = '10px';
            controlUI.style.cursor = 'pointer';
            controlUI.style.textAlign = 'center';
            controlUI.title = 'Click to center map on your location';
            controlDiv.appendChild(controlUI);

            ////
            var elem = document.createElement("img");
            elem.src = '{{ env('SITE_URL') }}' + "images/location.png";
            elem.setAttribute("height", "100%");
            elem.setAttribute("width", "100%");

            controlUI.appendChild(elem);

            // Setup the click event listeners to geolocate user
            google.maps.event.addDomListener(controlUI, 'click', geolocate);
        }

        function geolocate() {

            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(function (position) {

                    var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

                    // Create a marker and center map on user location
                    marker = new google.maps.Marker({
                        position: pos,
                        draggable: true,
                        animation: google.maps.Animation.DROP,
                        map: map
                    });

                    map.setCenter(pos);
                });
            }
        }

        $( document ).ready(function($)
        {
            $("#seleccionar-ubicacion").on('click', function ()
            {
                var center = map.getCenter();
                var lat = center.lat();
                var long = center.lng();
                console.log(lat + "," + long); //undefined

                document.getElementById("lat-ubicacion").value = lat;
                document.getElementById("lng-ubicacion").value = long;
            });
        });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key={{env('GOOGLE_API_KEY')}}&callback=initMap"
            async defer></script>
@endsection

@section('extra-js')
    <!--
    <script src="{{ asset('js/app.js') }}"></script>
    -->
@endsection