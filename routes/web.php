<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return redirect('lineas');
    } else {
        return view('auth/login');
    }

    //return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/register_cedula', 'Auth\RegisterController@registerCedula')->name('register-cedula');

//MIDDLEWARES
Route::middleware(['auth'])->group(function() {
    /*
    Route::get('map', function () {
        return view('map.index');
    });
    */

    //Map routes
    Route::resource('map', 'Map\MapController');
    Route::post('map-direction', 'Map\MapController@mapDirection')->name('map-direction');

    //Account routes
    Route::resource('perfil', 'Perfil\PerfilController');

    // Products routes
    Route::resource('productos', 'Producto\ProductoController');

    // Items routes
    Route::resource('items', 'Item\ItemController', ['only' => ['index', 'show']]);

    // Linea routes
    Route::resource('lineas', 'Linea\LineaController');
    Route::get('categorias/{linea}', 'Linea\LineaController@lineasCategoriaProductos')->name('lineas-categoria-productos');

    //Arindex routes
    Route::resource('arindex', 'Arindex\ArindexController', ['only' => ['index', 'show']]);

    //Cart routes
    Route::resource('cart', 'Cart\CartController');
    Route::get('addItem/{id}', 'Cart\CartController@addItem')->name('add-item');
    Route::get('clearCart', 'Cart\CartController@clearCart')->name('clear-cart');
    Route::get('addItemsGroup/{items}', 'Cart\CartController@addItemsGroup')->name('add-items-group');

    //Factura route
    Route::get('factura', function() {
        /*
        $cliente = \App\Models\ARCLIENTE::where("CODIGO", "9999999999")
            ->orWhere("CODIGO", "9999999999999")
            ->first();
        */
        $cliente = \Illuminate\Support\Facades\Auth::User();

        return view('cart.factura')->with('cliente', $cliente);
    })->name('factura');

    Route::resource('pedido', 'TBCINV\TBCINVController');
    Route::get('historial_pedidos', 'TBCINV\TBCINVController@historialPedidos')->name('historial-pedidos');
    Route::get('consultar_pedidos', 'TBCINV\TBCINVController@consultarPedidos')->name('consultar-pedidos');

    Route::resource('detalle_pedido', 'TBDINV\TBDINVController');

    //Pdf
    Route::resource('pdf', 'PDF\PdfController');
});