<?php

namespace App\Overrides;

use Illuminate\Database\Eloquent\Model;


class ModelCompositeKey extends Model
{
    //https://github.com/laravel/framework/issues/5517#issuecomment-157307903
    public $incrementing = false;

    /**
     * Set the keys for a save update query.
     * This is a fix for tables with composite keys
     * TODO: Investigate this later on
     * http://stackoverflow.com/a/36408026
     * https://github.com/laravel/framework/issues/5517#issuecomment-113655441
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(\Illuminate\Database\Eloquent\Builder $query) {
        if (is_array($this->primaryKey)) {
            foreach ($this->primaryKey as $pk) {
                $query->where($pk, '=', $this->original[$pk]);
            }
            return $query;
        }else{
            return parent::setKeysForSaveQuery($query);
        }
    }
}