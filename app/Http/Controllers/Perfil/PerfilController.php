<?php

namespace App\Http\Controllers\Perfil;

use App\Models\ARCLIENTE;
use App\Models\PERMISO;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use DateTime;

class PerfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cliente = Auth::user();

        //dd($cliente->CEDULA);
        return view('account.perfil_usuario')->with('cliente', $cliente);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();



            $cliente = $this->updateArcliente($request);

            if (is_null($cliente)) {
                DB::rollBack();

                session()->flash('alert_class', 'alert-danger');
                session()->flash('flash_message', 'Error al actualizar datos de usuario.');
                return redirect('perfil');
            }

            ########################## Sección donde se inserta permiso ##########################
            if (!$this->insertPermiso($request)) {
                DB::rollBack();

                session()->flash('alert_class', 'alert-danger');
                session()->flash('flash_message', 'Error al actualizar datos de usuario.');
                return redirect('perfil');
            }

            DB::commit();

            session()->flash('alert_class', 'alert-success'); //Tambien hay la clase alert-warning
            session()->flash('flash_message', 'Datos actualizados con exito');
            return redirect('perfil');
        } catch (\Exception $ex) {
            DB::rollBack();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateArcliente($request) {
        try {
            $cliente = ARCLIENTE::where('CODIGO', $request->identificacion)->first();

            $fecha = new DateTime();
            $fechaActual = $fecha->format('Y-m-d');

            $cliente->NOMBRE = $request->name;
            $cliente->DOMICILIO = $request->direccion;
            $cliente->FECHA_ULTMOD = $fechaActual;
            $cliente->APELLIDO = $request->apellido;
            $cliente->RAZONS = $request->nameapellido . " " . $request->name;

            $cliente->LEGAL = $request->name;
            $cliente->MAIL = $request->email;

            $cliente->CL_CELULAR = $request->telefono;

            $cliente->save();

            return $cliente;
        } catch (\Exception $ex) {
            //dd($ex->getMessage());
            return null;
        }
    }

    public function insertPermiso($request) {
        try {
            $fechaCreacion = new DateTime();
            $fechaCreacion = $fechaCreacion->format('Y-m-d H:i:s');

            $RDB_PERMISO = new PERMISO();
            $RDB_PERMISO->nombreTabla = "ARCLIENTE";
            $RDB_PERMISO->cliente_codigo = $request->identificacion;
            $RDB_PERMISO->origen = "099";
            $RDB_PERMISO->destino = "001";
            $RDB_PERMISO->estado = "0";
            $RDB_PERMISO->fechaCreacion = $fechaCreacion;
            $RDB_PERMISO->fechaModificacion = $fechaCreacion;

            $RDB_PERMISO->save();

            return true;
        } catch(\Exception $ex) {
            return false;
        }
    }
}
