<?php

namespace App\Http\Controllers\Producto;

use App\Models\ARBODB;
use App\Models\TBBIM_TIPOCL;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProductoController extends Controller
{
    public function index(Request $request) {
        try {
            $texto_busqueda = null;

            if (!is_null($request->nombre)) {
                $texto_busqueda = $request->nombre;
            }

            $cliente = Auth::user();
            $tipoclte = $cliente->TIPOCLTE;
            if ($tipoclte == "0") {
                $tipoclte = 1;
            }

            $nivel_precio_cliente = TBBIM_TIPOCL::find($tipoclte)->TIP_NIVELPRE;

            return view('productos.index')->with([
                'texto_busqueda' => $texto_busqueda,
                'nivel_precio_cliente' => $nivel_precio_cliente
            ]);
        } catch (\Exception $ex) {
            return view('errors.503');
        }
    }

    public function show($idProducto) {
        $producto = ARBODB::find($idProducto);

        if ($producto == null) {
            return view('errors.405');
        }

        return view('productos.productDetail', compact('producto', $producto));
    }
}
