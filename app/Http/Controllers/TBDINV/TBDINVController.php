<?php

namespace App\Http\Controllers\TBDINV;

use App\Models\TBCINV;
use App\Models\TBDINV;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TBDINVController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pedido = TBCINV::find($id);
        $detalles_pedido = TBDINV::where('DINV_CTINV', $id)->orderBy('DINV_LINEA')->get();
        
        //return view('pedido.detalle_pedido')->with('id', $id);
        return view('pedido.detalle_pedido', [
            'pedido' => $pedido,
            'detalles_pedido' => $detalles_pedido
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
