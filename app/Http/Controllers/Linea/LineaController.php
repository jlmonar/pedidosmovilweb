<?php

namespace App\Http\Controllers\Linea;

use App\Models\ARBODB;
use App\Models\ARINDEX;
use App\Models\TBBIM_TIPOCL;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LineaController extends Controller
{
    public function index(Request $request)
    {
        try {
            $texto_busqueda = null;

            if (!is_null($request->nombre)) {
                $texto_busqueda = $request->nombre;
            }

            return view('lineas.index')->with([
                'texto_busqueda' => $texto_busqueda
            ]);
        } catch (\Exception $ex) {
            return view('errors.503');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function lineasCategoriaProductos(Request $request, $linea) {
        $lineas = ARBODB::where('LINEA', '=', $linea)->distinct('GRUPO')->pluck('GRUPO')->toArray();

        $categorias = ARINDEX::where('DATA', 'G')
            ->whereIn('CODIGO', $lineas)
            ->orderBy('NOMBRE')
            ->get();

        if(sizeof($categorias) == 0) {
            $categorias = null;
        }

        $cliente = Auth::user();
        $tipoclte = $cliente->TIPOCLTE;
        if ($tipoclte == "0") {
            $tipoclte = 1;
        }

        $nivel_precio_cliente = TBBIM_TIPOCL::find($tipoclte)->TIP_NIVELPRE;

        $subcategoria = $request->categoria;

        $nombre_linea = ARINDEX::where('DATA', 'L')->where('CODIGO', $linea)->first();

        return view('lineas.categoriaProductos')->with([
            'categorias' => $categorias,
            'linea' => $linea,
            'nombre_linea' => $nombre_linea,
            'subcategoria' => $subcategoria,
            'nivel_precio_cliente' => $nivel_precio_cliente
        ]);
    }
}
