<?php

namespace App\Http\Controllers\Auth;

use App\Models\ARCLIENTE;
use App\Models\PERMISO;
use App\Models\TBBIT_ENT_SALD;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Utility\ValidarIdentificacion;

use DateTime;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'lineas';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'apellido' => 'required|string|max:255',
            'telefono' => 'numeric',
            'email' => 'required|string|email|max:255',
            'direccion' => 'required|string|max:1500',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        try {
            // Validaciones
            $this->validator($request->all())->validate();

            //dd('falta validar que cedula sea valida');

            DB::beginTransaction();

            $cliente = $this->insertArcliente($request);

            if (is_null($cliente)) {
                DB::rollBack();
                return redirect('register?identificacion=' . $request->identificacion)->withInput(Input::all())->withErrors(['Error' => 'Error durante registro. Error al insertar registro de CLIENTE.']);
            }

            if (!$this->insertTbbitEntSald($request)) {
                DB::rollBack();
                return redirect('register?identificacion=' . $request->identificacion)->withInput(Input::all())->withErrors(['Error' => 'Error durante registro. Error al insertar registro de TBNS.']);
            }

            ########################## Sección donde se inserta permiso ##########################
            if (!$this->insertPermiso($request)) {
                DB::rollBack();
                return redirect('register?identificacion=' . $request->identificacion)->withInput(Input::all())->withErrors(['Error' => 'Error durante registro. Error al insertar registro PRM.']);
            }

            DB::commit();

            $this->guard()->login($cliente);

            return $this->registered($request, $cliente)
                ?: redirect($this->redirectPath());
        } catch (\Exception $ex) {
            DB::rollBack();
            return redirect('register?identificacion=' . $request->identificacion)->withInput(Input::all())->withErrors(['Error' => 'Error inesperado durante el registro.']);
        }
    }

    public function registerCedula(Request $request) {
        return view('auth.register_cedula');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm(Request $request)
    {
        $identificacion = $request->identificacion;

        if (is_null($request->identificacion)) {
            return view('auth.register_cedula');
        }

        $arcliente = ARCLIENTE::where('CODIGO', $request->identificacion)->first();
        if (!is_null($arcliente)) {
            return redirect('register_cedula')->withInput(Input::all())->withErrors(['identificacion' => 'El numero de identificación ingresado ya se encuentra registrado.']);
        }

        if ($this->validarIdentificacion($identificacion)) {
            return view('auth.register')->with('identificacion', $identificacion);
        } else {
            return redirect('register_cedula')->withInput(Input::all())->withErrors(['identificacion' => 'El numero de identificación ingresado no es valido']);
        }
    }

    public function validarIdentificacion($identificacion) {
        $validador = new ValidarIdentificacion();

        if ($validador->validarCedula($identificacion)) {
            return true;
        }

        if ($validador->validarRucPersonaNatural($identificacion)) {
            return true;
        }

        if ($validador->validarRucSociedadPublica($identificacion)) {
            return true;
        }

        if ($validador->validarRucSociedadPrivada($identificacion)) {
            return true;
        }

        return false;
    }

    public function getTipoPersona($codigo) {
        if (strlen($codigo) == 13) {
            return "J";
        }

        return "N";
    }

    public function insertArcliente($request) {
        try {
            //$IMEI = $request->imei;

            $SEXO = "M";
            $FECHA_NACIMIENTO = NULL;
            $TELEFONO_DOMICILIO = "0";

            $RUC = "0";
            $CEDULA = "0";

            /*
            $cliente_json = $request->cliente_registro;
            $cliente_array = json_decode($cliente_json, TRUE);

            $data_usuario_json = $request->data_usuario;
            $data_usuario_array = json_decode($data_usuario_json, TRUE);
            */

            //dd($data_usuario_array);

            $fecha = new DateTime();
            $fechaActual = $fecha->format('Y-m-d');

            $TPERSONA = $this->getTipoPersona($request->identificacion);

            if (strlen($request->identificacion) == 13) {
                $RUC = $request->identificacion;
                $CEDULA = "0";
            } else {
                $CEDULA = $request->identificacion;
                $RUC = "0";
            }

            /*
            if (!is_null($data_usuario_array)) {
                $SEXO = ($data_usuario_array['sexo'] == "MASCULINO") ? "M": "F";
                $TELEFONO_DOMICILIO = $this->getTelefonoDomicilio($data_usuario_array);

                $fecha_string = $data_usuario_array['fechaNacimiento']; //CONVERTIR:fECHA A TIPO DATE
                $fecha_string = str_replace('/', '-', $fecha_string);
                $FECHA_NACIMIENTO = date('Y-m-d', strtotime($fecha_string));
            }
            */

            $cliente = new ARCLIENTE();

            $cliente->CODIGO = $request->identificacion;
            $cliente->NOMBRE = $request->name;
            $cliente->CEDULA = $CEDULA;
            $cliente->RUC = $RUC;
            $cliente->DOMICILIO = $request->direccion;
            $cliente->TELEFONOS = $TELEFONO_DOMICILIO;
            $cliente->DATA = "T";
            $cliente->CIUDAD = "GUAY";
            $cliente->FECHA_ULTMOD = $fechaActual;
            $cliente->FEC_APERTURA = $fechaActual;
            $cliente->ESTADO = "Activo";
            $cliente->FECHA_STATUS = $fechaActual;
            $cliente->FEC_ULTCOMP = null;
            $cliente->EXENTO_IVA = "N";
            $cliente->APELLIDO = $request->apellido;
            $cliente->COMENTARIO = "0";
            $cliente->SALDO_DEUDA = 0;
            $cliente->CUPO_MES = 0;
            $cliente->CUPO = 0;
            $cliente->PROVEEDOR = "C";
            $cliente->DESCUENTO = 0;
            $cliente->CODIGO1 = $request->codigo;
            $cliente->FPAGO = "001";
            $cliente->FDATA = "F";
            $cliente->VENDEDOR = "000";
            $cliente->VDATA = "V";
            $cliente->SALDOD_DEUDA = 0;
            $cliente->DIAS1 = 0;
            $cliente->DIAS2 = 0;
            $cliente->DIAS3 = 0;
            $cliente->DIAS4 = 0;
            $cliente->MONEDA = "D";
            $cliente->CTAMOV = NULL;
            $cliente->DIAS5 = 0;
            $cliente->DATAOC = "R";
            $cliente->CODIGOOC = "000";
            $cliente->DATAGC = "P";
            $cliente->CODIGOGC = "000";
            $cliente->DATAZO = "Z";
            $cliente->CODIGOZO = "09";
            $cliente->RAZONS = $request->nameapellido . " " . $request->name;
            $cliente->CTACONTABLE = NULL;
            $cliente->CODIGO_NUM = 0;
            $cliente->DIAS6 = 0;
            $cliente->DIAS7 = 0;
            $cliente->DIAS8 = 0;
            $cliente->TIPOCLTE = env('TIPOCL_CODIGO');
            $cliente->DESCPARAR = 0;
            $cliente->FAX = 0;
            $cliente->SALDO_COMPRAS = 0;
            $cliente->CUPO_COMPRAS = 0;
            $cliente->CUPOMES_COMPRAS = 0;
            $cliente->CL_NICK = NULL;
            $cliente->CL_ORDEN = NULL;
            $cliente->CL_CTACONT = NULL;
            $cliente->CL_EMPRESA = NULL;
            $cliente->DIA_VISITA = 0;
            $cliente->CONTROL = 0;
            $cliente->ST_COM = NULL;
            $cliente->LEGAL = $request->name;
            $cliente->MAIL = $request->email;
            $cliente->CL_NUM_PUESTO = ".";
            $cliente->CL_SEXO = $SEXO;
            $cliente->CL_DIRTRABAJO = null;
            $cliente->CL_ENVIO = "D";
            $cliente->CL_LUGTRABAJO = NULL;
            $cliente->CL_MAILTRABAJO = ".";
            $cliente->CL_CASILLA = "";
            $cliente->CL_FECNACIMIENTO = $FECHA_NACIMIENTO;
            $cliente->CL_CELULAR = $request->telefono;
            $cliente->CL_TELTRABAJO = "";
            $cliente->CL_PROMOCION = "0000";
            $cliente->CL_LLAMAR = "0000000";
            $cliente->CL_MERCADERIAN = "N";
            $cliente->CL_CUENTA = "A";
            $cliente->CL_CONTACTO = "";
            $cliente->SUELDO = 0;
            $cliente->OTROS_ING = 0;
            $cliente->ORIG_OTROS_ING = ".";
            $cliente->TIPO_VIVIENDA = "Hormigon";
            $cliente->VIVIENDA = "Propia";
            $cliente->CL_UBICACION = NULL;
            $cliente->CL_CDLACALLE = ".";
            $cliente->CL_MZBLOQUE = ".";
            $cliente->CL_NUMCASA = ".";
            $cliente->CL_TRANSVERSAL = ".";
            $cliente->DATA_TNEGOCIO = "TN";
            $cliente->TNEGOCIO = "000";
            $cliente->TPERSONA = $TPERSONA;
            $cliente->cargo_contac = ".";
            $cliente->cod_transporte = "000";
            $cliente->data_trans = "N";
            $cliente->saludo_cli = ".";
            $cliente->TARJETA = NULL;
            $cliente->PROFESION = NULL;
            $cliente->HORA_COBRO = "00:00";
            $cliente->COD_COBRADOR = "000";
            $cliente->DATA_CB = "CB";
            $cliente->PAIS = NULL;
            $cliente->CLASIFICA_CLIENTE = "1";
            $cliente->DATACLASIFICA_CLIENTE = NULL;
            $cliente->PERSONA_OCOMPRA = "";
            $cliente->PERSONA_RMERCAD = "";
            $cliente->UBC_ORIGEN = "001"; // SE CREA COMO UBC_ORIGEN 099 (?)

            // TODO INVESTIGAR COMO OBTENER IMEI DESDE NAVEGADOR MOVIL.
            //$cliente->IMEI = $IMEI;

            $cliente->save();

            return $cliente;
        } catch (\Exception $ex) {
            //dd($ex->getMessage());
            return null;
        }
    }

    public function insertTbbitEntSald($request) {
        try {
            $TBBIT_ENT_SALD = new TBBIT_ENT_SALD();
            $TBBIT_ENT_SALD->SECUENCIA = 1;
            $TBBIT_ENT_SALD->CODIGO = $request->identificacion;
            $TBBIT_ENT_SALD->EMPRESA = "1";
            $TBBIT_ENT_SALD->SALDO = 0;
            $TBBIT_ENT_SALD->SALDOD = 0;
            $TBBIT_ENT_SALD->CUPO = 0;
            $TBBIT_ENT_SALD->TIPO_ID = "C";

            $TBBIT_ENT_SALD->save();

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function insertPermiso($request) {
        try {
            $fechaCreacion = new DateTime();
            $fechaCreacion = $fechaCreacion->format('Y-m-d H:i:s');

            $RDB_PERMISO = new PERMISO();
            $RDB_PERMISO->nombreTabla = "ARCLIENTE";
            $RDB_PERMISO->cliente_codigo = $request->identificacion;
            $RDB_PERMISO->origen = "099";
            $RDB_PERMISO->destino = "001";
            $RDB_PERMISO->estado = "0";
            $RDB_PERMISO->fechaCreacion = $fechaCreacion;
            $RDB_PERMISO->fechaModificacion = $fechaCreacion;

            $RDB_PERMISO->save();

            return true;
        } catch(\Exception $ex) {
            return false;
        }
    }

    public function getTelefonoDomicilio($data_usuario_array) {
        if (!empty($data_usuario_array['telefono1'])) {
            return $data_usuario_array['telefono1'];
        }
        if (!empty($data_usuario_array['telefono2'])) {
            return $data_usuario_array['telefono2'];
        }
        if (!empty($data_usuario_array['telefono3'])) {
            return $data_usuario_array['telefono3'];
        }
        if (!empty($data_usuario_array['telefono4'])) {
            return $data_usuario_array['telefono4'];
        }
        if (!empty($data_usuario_array['telefono5'])) {
            return $data_usuario_array['telefono5'];
        }
        if (!empty($data_usuario_array['telefono6'])) {
            return $data_usuario_array['telefono6'];
        }
        return "0";
    }
}
