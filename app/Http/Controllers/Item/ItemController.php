<?php

namespace App\Http\Controllers\Item;

use App\Models\ARBODB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $productos = null;

        if (!is_null($request->linea) && !is_null($request->categoria)) {
            // Se hace el filtro por linea y categoria si ambos llegan como parametro
            if ($request->categoria == 'all') {
                // Si la categoria es all, retorno todos los tiems correspodientes a la linea.
                $productos = ARBODB::where('LINEA', $request->linea);
            } else {
                // Retorno productos según la linea y categoria enviados omo parámetro en el request.
                $productos = ARBODB::where('LINEA', $request->linea)
                    ->where('GRUPO', $request->categoria);
            }
        } else if (is_null($request->nombre)) {
            $productos = ARBODB::orderBy('DESCRIPCION', 'asc');
        } else {
            $productos = ARBODB::search($request->nombre)->orderBy('NUMERO_ITEM', 'DESC');
        }

        return $productos->paginate(40);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
}
