<?php

namespace App\Http\Controllers\TBCINV;

use App\Http\Controllers\APIController;
use App\Mail\SendPedidoEmail;
use App\Models\ARCLIENTE;
use App\Models\ARINDEX;
use App\Models\COORDENADA_DESTINO;
use App\Models\PERMISO;
use App\Models\PERMISO_LOTE;
use App\Models\TBBI_SECDOC;
use App\Models\TBBIM_TIPOCL;
use App\Models\TBBIT_ENT_SALD;
use App\Models\TBCINV;
use App\Models\TBDINV;
use App\Models\TBDINVD;
use App\Models\TBPARAM;
use App\Utility\ValidarIdentificacion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use Cart;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

//use Symfony\Component\HttpFoundation\File\File;

class TBCINVController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function sendEmail($cliente, $vendedor) {
        try {
            //Aquí genero el PDF.
            $cartCollection = Cart::getContent();
            $data = array_reverse($cartCollection->toArray());
            $date = date('Y-m-d');
            $invoice = "2222";
            $view =  \View::make('pdf.pedido', compact('data', 'date', 'invoice'))->render();
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($view)->setPaper('a4', 'landscape')->setWarnings(false)->save('pdf-pedidos/pedido_'.$cliente->CODIGO.'.pdf');

            //Se envia el email al vendedor.
            Mail::to($vendedor->NUMERO)->send(new SendPedidoEmail('pdf-pedidos/pedido_'.$cliente->CODIGO.'.pdf'));


        } catch (\Exception $ex) {
            //return false;
        } finally {
            //Se borra el pdf generado.
            if (\File::exists('pdf-pedidos/pedido_'.$cliente->CODIGO.'.pdf')) {
                \File::delete('pdf-pedidos/pedido_'.$cliente->CODIGO.'.pdf');
            }
            return true;
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $bodega_web = env('BODEGA_WEB');
            $ubc_origen = env('UBC_ORIGEN');

            $lote = $this->get_numero_lote($ubc_origen);
            DB::beginTransaction();

            /*
             * TODO SE DEBE VALIDAR QUE MONTO TOTAL DEL CARRITO NO SEA MAYOR A 200$ CUANDO ES UN CONSUMIDOR FINAL
             */

            $cliente = null;
            $cliente_logueado = Auth::user();
            if (is_null($cliente_logueado)) {
                DB::rollBack();
                //return $this->errorResponse("No se recibió datos del cliente logueado en el requerimiento", 400);
                return view('errors.503');
            }

            // Valido que lleguen todos los datos de facturación del cliente en el request.
            $validacionCampos = $this->validarCamposCliente($request);
            if (is_string($validacionCampos)) {
                //return $this->errorResponse($validacionCampos, 400);
                DB::rollBack();
                return view('errors.503');
            }

            // Verifico que la cédula recibida como datos de facturación sea válida (Verificación se realiza solo si identificación recibida no es CONSUMIDOR FINAL).
            if ($request["identificacion"] != "9999999999" && $request["identificacion"] != "9999999999999") {
                if (!$this->validarIdentificacion($request["identificacion"])) {
                    //return $this->errorResponse("La identificación recibida como datos de facturación del cliente no es valida.", 400);
                    return view('errors.503');
                }
            }

            // Verifico que la identificación del cliente logueado sea válida.
            if (!$this->validarIdentificacion($cliente_logueado->CODIGO)) {
                //return $this->errorResponse("La identificación recibida del cliente logueado no es valida.", 400);
                return view('errors.503');
            }

            if ($request["identificacion"] == "9999999999" or $request["identificacion"] == "9999999999999") {
                // Cliente recibido es consumidor final.
                $cliente = ARCLIENTE::where("CODIGO", "9999999999")
                    ->orWhere("CODIGO", "9999999999999")
                    ->first();

                if (is_null($cliente)) {
                    DB::rollBack();
                    //return $this->errorResponse("No se encontró cliente consumidor final.", 404);
                    return view('errors.503');
                }

                /*
                $cliente_logueado = ARCLIENTE::where("CEDULA", $request->cliente_logueado)
                    ->orWhere("RUC", $request->cliente_logueado)
                    ->first();

                if (is_null($cliente_logueado)) {
                    DB::rollBack();
                    return $this->errorResponse("No se encontró cliente logueado con la identificacion " . $request->cliente_logueado . ".", 404);
                }
                */
            } else {
                $cliente = ARCLIENTE::where("CEDULA", $request["identificacion"])
                    ->orWhere("RUC", $request["identificacion"])
                    ->first();

                if (!is_null($cliente)) {
                    //Cliente si existe, por lo tanto actualizo los datos del cliente con los datos recibidos en el requerimiento.
                    $cliente->NOMBRE = $request["name"];
                    $cliente->APELLIDO = $request["apellido"];
                    $cliente->DOMICILIO = $request["direccion"];
                    $cliente->LEGAL = $request["name"];
                    $cliente->RAZONS = $request["apellido"] . " " . $request["name"];
                    $cliente->MAIL = $request["email"];
                    $cliente->CL_CELULAR = $request["telefono"];

                    $cliente->save();
                } else {
                    //Cliente no existe, hay que crear nuevo registro con los datos recibidos en el request.
                    $cliente = $this->insertArcliente($request);
                    if (is_null($cliente)) {
                        DB::rollBack();
                        //return $this->showMessage("Error al insertar registro de CLIENTE", 500);
                        return view('errors.503');
                    }

                    if (!$this->insertTbbitEntSald($request)) {
                        DB::rollBack();
                        //return $this->showMessage("Error al insertar registro TBBIT_ENT_SALD", 500);
                        return view('errors.503');
                    }
                }

                if (!$this->insertPermiso($request["identificacion"])) {
                    DB::rollBack();
                    //return $this->showMessage("Error al insertar registro PERMISO de cliente en el servidor", 500);
                    return view('errors.503');
                }
            }


            //Obtengo nivel de precio del cliente.
            $tipoclte = $cliente->TIPOCLTE;
            if ($tipoclte == "0") {
                $tipoclte = "1";
            }

            $tipo_cliente = TBBIM_TIPOCL::find($tipoclte);
            $nivel_precio_cliente = $tipo_cliente->TIP_NIVELPRE;
            if (is_null($nivel_precio_cliente)) {
                //return $this->errorResponse("Error al obtener nivel de precio del cliente. ", 404);
                DB::rollBack();
                return view('errors.503');
            }

            $cinv_sec = TBBI_SECDOC::find('CINV_SEC');
            if (is_null($cinv_sec)) {
                DB::rollBack();
                //return $this->errorResponse("Error al obtener secuencia del pedido. ", 404);
                return view('errors.503');
            }

            $items_array = Cart::getContent();
            if (is_null($items_array)) {
                DB::rollBack();
                //return $this->errorResponse("La cantidad de items del pedido no puede ser cero.", 404);
                return view('errors.503');
            }
            /*
             * TODO Aquí se debe cambiar los precios según el nivel del precio del cliente que se envio en el formulario de la factura, si resulta ser el mismo logueado entonces no hay que cambuiar nada.
             */

            $vendedor = $this->obtenerVendedorMasCercano();
            if (is_null($vendedor)) {
                DB::rollBack();
                return $this->errorResponse("Error al bucar el vendedor mas cercano a su ubicación  de origen.", 500);
            }

            //En lugar de $cliente_logueado deberia ser solo $cliente?
            if (!$this->insertCoordenadaDestino($request['identificacion'], $cliente_logueado)) {
                DB::rollBack();
                return view('errors.503');
                //return $this->errorResponse("Error al guardar la ubicación de destino", 500);
            }

            $tbcinv = $this->insertTbcinv($cliente, $cinv_sec, $bodega_web, $items_array, $nivel_precio_cliente, $lote, $ubc_origen, $vendedor, $request["opcion_entrega"]);
            if (is_string($tbcinv)) {
                DB::rollBack();
                //return $this->errorResponse($tbcinv, 404);
                return view('errors.503');
            }

            //Actualizo secuencial de la cabecera
            $cinv_sec->DOC_NUM = $cinv_sec->DOC_NUM + 1;
            $cinv_sec->save();

            ########################## Sección donde se inserta detalles del pedido ##########################
            //Parametro Desglosa IVA, si es S se multiplican valores por el IVA 12%, caso contrario queda solo el valor neto.
            $desglosa_iva = TBPARAM::find(1);
            if (is_null($desglosa_iva)) {
                DB::rollBack();
                //return $this->errorResponse("Error al obtener parametro deslosa iva. ", 404);
                return view('errors.503');
            }

            $dinv_sec = TBBI_SECDOC::find('DINV_SEC');
            if (is_null($dinv_sec)) {
                DB::rollBack();
                //return $this->errorResponse("Error al obtener secuencia de los detalles del pedido. ", 404);
                return view('errors.503');
            }

            $linea = 1;
            //Inserción de detalles del pedido
            foreach ($items_array as $item) {
                $tbdinv = $this->insertTbdinv($cliente, $dinv_sec, $cinv_sec->DOC_NUM, $item, $linea, $desglosa_iva,$bodega_web, $nivel_precio_cliente);
                if (is_string($tbdinv)) {
                    DB::rollBack();
                    //return $this->errorResponse($tbdinv, 404);
                    return view('errors.503');
                }

                $tbdinvd = $this->insertTbdinvd($cliente, $dinv_sec, $cinv_sec->DOC_NUM, $item, $desglosa_iva);
                if (is_string($tbdinvd)) {
                    DB::rollBack();
                    //return $this->errorResponse($tbdinvd, 404);
                    return view('errors.503');
                }
                //Actualizo secuencial del detalle
                $dinv_sec->DOC_NUM = $dinv_sec->DOC_NUM + 1;
                $dinv_sec->save();

                $linea = $linea + 1;
            }

            ########################## Sección donde se inserta permiso lote ##########################
            $permiso_lote = $this->insertPermisoLote($lote, "TBCINV", 1, $ubc_origen, "001");
            if (is_string($permiso_lote)) {
                DB::rollBack();
                //return $this->errorResponse($permiso_lote, 404);
                return view('errors.503');
            }

            ///// ENVIO CORREO AL VENDEDOR con el pdf adjunto
            $cliente_envio_mail = $cliente;
            if ($request["identificacion"] == "9999999999" or $request["identificacion"] == "9999999999999") {
                $cliente_envio_mail = $cliente_logueado;
            }
            //TODO Aqui hay que modificar cuerpo del mensaje para que incluya datos del vendedor, cliente, y coordenadas de destino.
            $this->sendEmail($cliente_envio_mail, $vendedor);

            //Se borra contenido del carrito de compras.
            Cart::clear();
            DB::commit();

            //////////////////
            /*
            dd($vendedor);
            DB::rollBack();
            dd('validado');
            */
            //////////////////

            session()->flash('flash_message', 'Su pedido ha sido realizado con éxito.');
            session()->flash('alert_class', 'alert-success');
            return redirect()->route('productos.index');
        } catch (\Exception $ex) {
            DB::rollBack();
            //return $this->showMessage("Excepcion ocurrida: " . $ex->getMessage(), 200);
            return view('errors.503');
        }
    }

    private function insertTbcinv($cliente, $cinv_sec, $bodega_web, $items_array, $nivel_precio_cliente, $lote, $ubc_origen, $vendedor, $opcion_entrega) {
        try {
            $fecha = new DateTime();
            $fechaActual = $fecha->format('Y-m-d');
            $fechaActualHoras = $fecha->format('Y-m-d H:i:s');

            $identificacion_usuario = "00";
            if (strlen($cliente->CEDULA) == 10) {
                $identificacion_usuario = $cliente->CEDULA;
            } else if (strlen($cliente->RUC) == 13) {
                $identificacion_usuario = $cliente->RUC;
            }

            $TBCINV = new TBCINV();
            $TBCINV->CINV_SEC = $cinv_sec->DOC_NUM +1;
            $TBCINV->CINV_TDOC = "PV";
            $TBCINV->CINV_NUM = $cinv_sec->DOC_NUM +1;
            $TBCINV->CINV_BOD = $bodega_web;
            $TBCINV->CINV_TBOD = "O";
            $TBCINV->CINV_REF = "000";
            $TBCINV->CINV_FECING = $fechaActual;
            $TBCINV->CINV_ID = $identificacion_usuario;
            $TBCINV->CINV_NOMID = $cliente->RAZONS;
            $TBCINV->CINV_DSC = 0.0;
            $TBCINV->CINV_COM1 = ".";
            $TBCINV->CINV_COM2 = ".";
            $TBCINV->CINV_COM3 = ".";
            $TBCINV->CINV_COM4 = ".";
            $TBCINV->CINV_TASA = 1.0;
            $TBCINV->CINV_LOGIN = "OPTIMUS WEB";
            $TBCINV->CINV_TDIV = "D";
            $TBCINV->CINV_ST = "A";
            $TBCINV->EMPRESA = "1";
            $TBCINV->DATATR = "N";
            $TBCINV->CODIGOTR = "000";
            $TBCINV->CINV_NOTA = ".";
            $TBCINV->CINV_FPAGO = "C";
            $TBCINV->CINV_HORA = $fechaActualHoras;
            $TBCINV->CINV_TIPRECIO = $nivel_precio_cliente;
            $TBCINV->DATA_EMB = "EM";
            $TBCINV->COD_EMBALADOR = "000";
            $TBCINV->CARTONES = "0";
            $TBCINV->FUNDAS = "0";
            $TBCINV->SACOS = "0";
            $TBCINV->CINV_STPRO = "0";
            $TBCINV->CINV_CEDULA = $identificacion_usuario;
            $TBCINV->CINV_FONOS = $cliente->TELEFONOS;
            $TBCINV->CINV_DIREC = $cliente->DOMICILIO;
            $TBCINV->F_DATA = "F";
            $TBCINV->F_PAGOC = "001";
            $TBCINV->CINV_VENDEDOR = $vendedor->CODIGO;
            $TBCINV->CINV_VDATA = "V";
            $TBCINV->CINV_DIAS = "0";
            $TBCINV->CINV_VCTO1 = ".";
            $TBCINV->CINV_VCTO2 = ".";
            $TBCINV->CINV_VCTO3 = ".";
            $TBCINV->CINV_NUMAPROBACION = "0";
            $TBCINV->CINV_PROBLEMAS = "N";
            $TBCINV->ST_NPC = "1";
            $TBCINV->CINV_ST_EXTERIOR = "0";
            $TBCINV->cinv_fecVencto = $fechaActual;//sin horas
            $TBCINV->codigodp = $opcion_entrega; // Por ahora la opción de compra la guardo en este campo.
            $TBCINV->FEC_REGISTRO = $fechaActualHoras;//con horas
            $TBCINV->MAQUINA = "ANDROID DEVICE";
            $TBCINV->LMODULO = "0";
            $TBCINV->CINV_ITEMS = count($items_array);
            $TBCINV->cinv_items_sn = "0";
            $TBCINV->CINV_ALMACEN = "1";
            $TBCINV->cinv_campana = "0";
            $TBCINV->cinv_largo_cab = "1";
            $TBCINV->CINV_NAVE_DATA = "NV";
            $TBCINV->CINV_DESTINO_DATA = "PU";
            $TBCINV->CINV_NAVIERA_DATA = "NA";
            $TBCINV->CINV_NAVE = "000";
            $TBCINV->CINV_DESTINOE = "000";
            $TBCINV->CINV_NAVIERA = "000";
            $TBCINV->CINV_BOOKING = "0";
            $TBCINV->UBC_ORIGEN = $ubc_origen;
            $TBCINV->ID_COMUNICACIONES = $lote;
            $TBCINV->save();

            return $TBCINV;
        } catch (\Exception $ex) {
            return "Error al insertar cabecera del pedido em base remota.";
        }
    }

    private function insertTbdinv($cliente, $dinv_sec, $cinv_sec, $item, $linea, $desglosa_iva, $bodega_web, $nivel_precio_cliente) {
        try {
            $fecha = new DateTime();
            $fechaActual = $fecha->format('Y-m-d');

            $TBDINV = new TBDINV();
            $TBDINV->DINV_SEC = $dinv_sec->DOC_NUM + 1;
            $TBDINV->DINV_CTINV = $cinv_sec;
            $TBDINV->DINV_ITEM = $item->id;
            $TBDINV->DINV_LINEA = $linea;
            $TBDINV->DINV_CANT = $item->quantity;

            if (strcmp($desglosa_iva->PARAM_VA,"S") == 0) {
                $TBDINV->DINV_DSC = $item->price * $item->quantity * ($cliente->DESCUENTO/100) / 1.12;
                $TBDINV->DINV_VTA = $item->price * $item->quantity / 1.12;
                $TBDINV->DINV_COS = $item->price * $item->quantity / 1.12;
                $TBDINV->DINV_PRECIO_REAL = $item->price  / 1.12;

            } else {
                $TBDINV->DINV_DSC = $item->price * $item->quantity * ($cliente->DESCUENTO/100);
                $TBDINV->DINV_VTA = $item->price * $item->quantity;
                $TBDINV->DINV_COS = $item->price * $item->quantity;
                $TBDINV->DINV_PRECIO_REAL = $item->price;
            }
            $TBDINV->DINV_IVA = ($TBDINV->DINV_VTA - $TBDINV->DINV_DSC)*0.12;

            $TBDINV->DINV_ICE = 0.0;
            $TBDINV->DINV_PRCT_DSC = $cliente->DESCUENTO;
            $TBDINV->DINV_DSC_EX = 0.0;
            $TBDINV->DINV_BOD = $bodega_web;
            $TBDINV->DINV_TBOD = "O";
            $TBDINV->DINV_FECHA = $fechaActual;
            $TBDINV->DINV_CANT2 = 0.0;
            //$TBDINV->DINV_PRECIO_REAL = $TBDINV->DINV_VTA;
            $TBDINV->EMPRESA = "1";
            $TBDINV->PROMOCION = 0.0;
            $TBDINV->DINV_DCTOPRO = 0.0;
            $TBDINV->DINV_VALPRO = 0.0;
            $TBDINV->DINV_DETALLEDSCTO = "" . round($cliente->DESCUENTO) . "%";
            $TBDINV->DINV_DESCRIPCION = "BODEGA WEB=>" . $item->quantity;
            $TBDINV->STOCK1 = $item["price"];
            $TBDINV->BODEGA1 = round($cliente->DESCUENTO);
            $TBDINV->CAN_PED = 0;
            $TBDINV->DINV_PRCT_DSC_ANTES = 0.0;
            $TBDINV->DINV_PRECIO_ANTES = 0.0;
            $TBDINV->NIV_PRECIO = $nivel_precio_cliente;
            $TBDINV->DINV_ESTADO = "P";
            $TBDINV->DINV_FALTANTE = 0.0;
            $TBDINV->DINV_REVERSAR = 0.0;
            $TBDINV->dinv_dirent = ".";
            $TBDINV->DINV_CANTAPROBADA = 0.0;
            $TBDINV->DINV_DESC_ITEM = "BODEGA WEB=>" . $item->quantity;
            $TBDINV->DINV_DESC_LARGA = 0;
            $TBDINV->DINV_LMODIF = 0;
            $TBDINV->dinv_bulto = 0.0;
            $TBDINV->dinv_largo = 0.0;
            $TBDINV->ST_OBSEQUIO = 0;
            $TBDINV->ST_PRODUCTO = -1;
            $TBDINV->IRBP_DETPED = 0.0;
            $TBDINV->ICE_DETPED = 0.0;

            $TBDINV->save();
        } catch (\Exception $ex) {
            return "Error al insertar detalle del pedido, error en el item: " . $item->name;
        }
    }

    private function insertTbdinvd($cliente, $dinv_sec, $cinv_sec, $item, $desglosa_iva) {
        try {
            $TBDINVD = new TBDINVD();
            $TBDINVD->DINVD_CTINV = $cinv_sec;
            $TBDINVD->DINVD_TDIV = "D";
            $TBDINVD->DINVD_DINV = $dinv_sec->DOC_NUM + 1;

            if (strcmp($desglosa_iva->PARAM_VA,"S") == 0) {
                $TBDINVD->DINVD_DSC = $item->price * $item->quantity * ($cliente->DESCUENTO/100) / 1.12;
                $TBDINVD->DINVD_VTA = $item->price * $item->quantity / 1.12;
                $TBDINVD->DINVD_COS = $item->price * $item->quantity / 1.12;

            } else {
                $TBDINVD->DINVD_DSC = $item->price * $item->quantity * ($cliente->DESCUENTO/100);
                $TBDINVD->DINVD_VTA = $item->price * $item->quantity;
                $TBDINVD->DINVD_COS = $item->price * $item->quantity;
            }
            $TBDINVD->DINVD_IVA = ($TBDINVD->DINVD_VTA - $TBDINVD->DINVD_DSC) * 0.12;;
            $TBDINVD->DINVD_ICE = 0.0;
            $TBDINVD->DINVD_DSC_EX = 0.0;
            $TBDINVD->DINVD_PRECIO_REAL = null;
            $TBDINVD->DINVD_VALPRO = 0.0;

            $TBDINVD->save();
        } catch (\Exception $ex) {
            return "Error al insertar detalle del pedidod, error en el item: " . $item["name"]. " error: " . $ex->getMessage();
        }
    }

    private function insertPermisoLote($lote, $tabla, $cantidad, $ubc_origen, $destino) {
        try {
            $fechaCreacion = new DateTime();
            $fechaCreacion = $fechaCreacion->format('Y-m-d H:i:s');
            $rdb_permiso_lote = new PERMISO_LOTE();
            $rdb_permiso_lote->FECHA_SUBIDA = $fechaCreacion;
            $rdb_permiso_lote->TABLA = $tabla;
            $rdb_permiso_lote->CANTIDAD = $cantidad;
            $rdb_permiso_lote->ID_LOTE = $lote;
            $rdb_permiso_lote->ORIGEN = $ubc_origen;
            $rdb_permiso_lote->DESTINO = $destino;
            $rdb_permiso_lote->STATUS = "0";
            if (!is_null($rdb_permiso_lote)) {
                $rdb_permiso_lote->save();
            } else {
                return "Error al insertar permiso lote en base remota. lote: " . $lote;
            }
        } catch(\Exception $ex) {
            return "Error al insertar permiso lote en base remota. lote: " . $lote;
        }
    }

    private function get_numero_lote($ubc_origen) {
        try {
            $numeroLote = $ubc_origen . date('d') . date('m') . date('Y') . date('h') . date('i');
            $aleatorio = rand(1, 100);
            $numeroLote = $numeroLote . $aleatorio;
        } catch (\Exception $e) {
            Log::error("\nArchivo: " . $e->getFile() .
                "\nLínea: " . $e->getLine() .
                "\nBase : Remota" .
                "\nFuncion: get_numero_lote" .
                "\nMensaje: " . $e->getMessage());
            return false;
        }
        return $numeroLote;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function historialPedidos() {
        return view('pedido.historial_pedidos');
    }

    public function consultarPedidos(Request $request) {


        $initial_date_formatted = date_create_from_format('d/m/Y', $request->fecha_inicio);
        $fecha_inicio = $initial_date_formatted->format('Y-m-d');

        $end_date_formatted = date_create_from_format('d/m/Y', $request->fecha_fin);
        $fecha_fin = $end_date_formatted->format('Y-m-d');


        $cliente = Auth::user();
        $pedidos = TBCINV::whereBetween('CINV_FECING', array($fecha_inicio, $fecha_fin))
            ->where('CINV_ID', $cliente->CODIGO)
            ->orderBy('CINV_FECING', 'DESC')
            ->paginate(10);

        return view('pedido.historial_pedidos')->with('pedidos', $pedidos);
    }

    ///////////////////////////////////////// Funciones nuevas /////////////////////////////////////////
    public function validarCamposCliente($request) {
        if ($request["identificacion"] == null) {
            return "No se recibió identificación del cliente en el requerimiento.";
        }

        if ($request["name"] == null) {
            return "No se recibió apellido del cliente en el requerimiento.";
        }

        if ($request["apellido"] == null) {
            return "No se recibió apellido del cliente en el requerimiento.";
        }

        if ($request["email"] == null) {
            return "No se recibió correo electrónico del cliente en el requerimiento.";
        }

        if ($request["direccion"] == null) {
            return "No se recibió dirección de domicilio del cliente en el requerimiento.";
        }

        if ($request["telefono"] == null) {
            return "No se recibió telefono del cliente en el requerimiento.";
        }

        if ($request["opcion_entrega"] == null) {
            return "No se recibió opcion de entrega del cliente en el requerimiento.";
        }

        return true;
    }

    public function validarIdentificacion($identificacion) {
        $validador = new ValidarIdentificacion();

        if ($validador->validarCedula($identificacion)) {
            return true;
        }

        if ($validador->validarRucPersonaNatural($identificacion)) {
            return true;
        }

        if ($validador->validarRucSociedadPublica($identificacion)) {
            return true;
        }

        if ($validador->validarRucSociedadPrivada($identificacion)) {
            return true;
        }

        return false;
    }

    public function insertArcliente($request) {
        try {
            $SEXO = "M";
            $FECHA_NACIMIENTO = NULL;
            $TELEFONO_DOMICILIO = "0";

            $RUC = "0";
            $CEDULA = "0";

            $fecha = new DateTime();
            $fechaActual = $fecha->format('Y-m-d');

            $TPERSONA = $this->getTipoPersona($request["identificacion"]);

            if (strlen($request["identificacion"]) == 13) {
                $RUC = $request["identificacion"];
                $CEDULA = "0";
            } else {
                $CEDULA = $request["identificacion"];
                $RUC = "0";
            }

            $cliente = new ARCLIENTE();

            $cliente->CODIGO = $request["identificacion"];
            $cliente->NOMBRE = $request["name"];
            $cliente->CEDULA = $CEDULA;
            $cliente->RUC = $RUC;
            $cliente->DOMICILIO = $request["direccion"];
            $cliente->TELEFONOS = $TELEFONO_DOMICILIO;
            $cliente->DATA = "T";
            $cliente->CIUDAD = "GUAY";
            $cliente->FECHA_ULTMOD = $fechaActual;
            $cliente->FEC_APERTURA = $fechaActual;
            $cliente->ESTADO = "Activo";
            $cliente->FECHA_STATUS = $fechaActual;
            $cliente->FEC_ULTCOMP = null;
            $cliente->EXENTO_IVA = "N";
            $cliente->APELLIDO = $request["apellido"];
            $cliente->COMENTARIO = "0";
            $cliente->SALDO_DEUDA = 0;
            $cliente->CUPO_MES = 0;
            $cliente->CUPO = 0;
            $cliente->PROVEEDOR = "C";
            $cliente->DESCUENTO = 0;
            $cliente->CODIGO1 = $request["identificacion"];
            $cliente->FPAGO = "001";
            $cliente->FDATA = "F";
            $cliente->VENDEDOR = "000";
            $cliente->VDATA = "V";
            $cliente->SALDOD_DEUDA = 0;
            $cliente->DIAS1 = 0;
            $cliente->DIAS2 = 0;
            $cliente->DIAS3 = 0;
            $cliente->DIAS4 = 0;
            $cliente->MONEDA = "D";
            $cliente->CTAMOV = NULL;
            $cliente->DIAS5 = 0;
            $cliente->DATAOC = "R";
            $cliente->CODIGOOC = "000";
            $cliente->DATAGC = "P";
            $cliente->CODIGOGC = "000";
            $cliente->DATAZO = "Z";
            $cliente->CODIGOZO = "09";
            $cliente->RAZONS = $request["apellido"] . " " . $request["name"];
            $cliente->CTACONTABLE = NULL;
            $cliente->CODIGO_NUM = 0;
            $cliente->DIAS6 = 0;
            $cliente->DIAS7 = 0;
            $cliente->DIAS8 = 0;
            $cliente->TIPOCLTE = env('TIPOCL_CODIGO');
            $cliente->DESCPARAR = 0;
            $cliente->FAX = 0;
            $cliente->SALDO_COMPRAS = 0;
            $cliente->CUPO_COMPRAS = 0;
            $cliente->CUPOMES_COMPRAS = 0;
            $cliente->CL_NICK = NULL;
            $cliente->CL_ORDEN = NULL;
            $cliente->CL_CTACONT = NULL;
            $cliente->CL_EMPRESA = NULL;
            $cliente->DIA_VISITA = 0;
            $cliente->CONTROL = 0;
            $cliente->ST_COM = NULL;
            $cliente->LEGAL = $request["name"];
            $cliente->MAIL = $request["email"];
            $cliente->CL_NUM_PUESTO = ".";
            $cliente->CL_SEXO = $SEXO;
            $cliente->CL_DIRTRABAJO = null;
            $cliente->CL_ENVIO = "D";
            $cliente->CL_LUGTRABAJO = NULL;
            $cliente->CL_MAILTRABAJO = ".";
            $cliente->CL_CASILLA = "";
            $cliente->CL_FECNACIMIENTO = $FECHA_NACIMIENTO;
            $cliente->CL_CELULAR = $request["telefono"]; //Aquí va el numero que ingresó el usuario en la aplicación.
            $cliente->CL_TELTRABAJO = "";
            $cliente->CL_PROMOCION = "0000";
            $cliente->CL_LLAMAR = "0000000";
            $cliente->CL_MERCADERIAN = "N";
            $cliente->CL_CUENTA = "A";
            $cliente->CL_CONTACTO = "";
            $cliente->SUELDO = 0;
            $cliente->OTROS_ING = 0;
            $cliente->ORIG_OTROS_ING = ".";
            $cliente->TIPO_VIVIENDA = "Hormigon";
            $cliente->VIVIENDA = "Propia";
            $cliente->CL_UBICACION = NULL;
            $cliente->CL_CDLACALLE = ".";
            $cliente->CL_MZBLOQUE = ".";
            $cliente->CL_NUMCASA = ".";
            $cliente->CL_TRANSVERSAL = ".";
            $cliente->DATA_TNEGOCIO = "TN";
            $cliente->TNEGOCIO = "000";
            $cliente->TPERSONA = $TPERSONA;
            $cliente->cargo_contac = ".";
            $cliente->cod_transporte = "000";
            $cliente->data_trans = "N";
            $cliente->saludo_cli = ".";
            $cliente->TARJETA = NULL;
            $cliente->PROFESION = NULL;
            $cliente->HORA_COBRO = "00:00";
            $cliente->COD_COBRADOR = "000";
            $cliente->DATA_CB = "CB";
            $cliente->PAIS = NULL;
            $cliente->CLASIFICA_CLIENTE = "1";
            $cliente->DATACLASIFICA_CLIENTE = NULL;
            $cliente->PERSONA_OCOMPRA = "";
            $cliente->PERSONA_RMERCAD = "";
            $cliente->UBC_ORIGEN = "001"; // SE CREA COMO UBC_ORIGEN 099 (?)

            $cliente->save();

            return $cliente;
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function insertTbbitEntSald($request) {
        try {
            $TBBIT_ENT_SALD = new TBBIT_ENT_SALD();
            $TBBIT_ENT_SALD->SECUENCIA = 1;
            $TBBIT_ENT_SALD->CODIGO = $request['identificacion'];
            $TBBIT_ENT_SALD->EMPRESA = "1";
            $TBBIT_ENT_SALD->SALDO = 0;
            $TBBIT_ENT_SALD->SALDOD = 0;
            $TBBIT_ENT_SALD->CUPO = 0;
            $TBBIT_ENT_SALD->TIPO_ID = "C";

            $TBBIT_ENT_SALD->save();

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function insertPermiso($codigo_cliente) {
        try {
            $fechaCreacion = new DateTime();
            $fechaCreacion = $fechaCreacion->format('Y-m-d H:i:s');

            $RDB_PERMISO = new PERMISO();
            $RDB_PERMISO->nombreTabla = "ARCLIENTE";
            $RDB_PERMISO->cliente_codigo = $codigo_cliente;
            $RDB_PERMISO->origen = "099";
            $RDB_PERMISO->destino = "001";
            $RDB_PERMISO->estado = "0";
            $RDB_PERMISO->fechaCreacion = $fechaCreacion;
            $RDB_PERMISO->fechaModificacion = $fechaCreacion;

            $RDB_PERMISO->save();

            return true;
        } catch(\Exception $ex) {
            //dd($ex->getMessage());
            return false;
        }
    }

    /**
     * @param $request
     * @return null
     */
    public function obtenerVendedorMasCercano() {
        try {
            $latitudOrigen = Session::get('latitud');
            $longitudOrigen = Session::get('longitud');
            // TODO Hay que validar que existe la coordenada destino en la SESION.

            $vendedores = ARINDEX::select('CODIGO', 'DATA', 'NOMBRE', 'NUMERO')
                ->where('DATA', "V")
                ->get();

            $url = new \SoapClient("http://www.corporacionsmartest.com/wsSmartestLocationForDesktop/wsSmartestLocation.php?wsdl");

            $vendedor_cercano= null;
            $distancia_mas_corta = 0;

            foreach ($vendedores as $vendedor) {
                $ultimoPuntoResponse = $url->__call("UltimoPuntoInsertadoPorNumeroTelefono", array('telefono' => $vendedor->CODIGO));

                if (!empty($ultimoPuntoResponse)) {
                    $result = $ultimoPuntoResponse[0];
                    $latitudDestino = $result->latitud;
                    $longitudDestino = $result->longitud;

                    $distance = $this->haversineGreatCircleDistance(
                        $latitudOrigen, $longitudOrigen,
                        $latitudDestino, $longitudDestino
                    );
                    if ($distance < $distancia_mas_corta || $distancia_mas_corta == 0) {
                        $distancia_mas_corta = $distance;
                        $vendedor_cercano = $vendedor;
                    }
                }
            }

            return $vendedor_cercano;
        } catch(\Exception $ex) {
            Log::error("\nExcepción: " . $ex->getMessage());
            return null;
        }
    }

    /**
     * @param $codigo_cliente  Código del cliente que va a tener los datos de facturación.
     * @param $cliente_logueado  Cliente logueado en la app.
     * @return bool
     */
    public function insertCoordenadaDestino($codigo_cliente, $cliente_logueado) {
        try {
            if ($codigo_cliente == "9999999999" or $codigo_cliente == "9999999999999") {
                $codigo_cliente = $cliente_logueado->CODIGO;
            }

            $latitudDestino = Session::get('latitud');
            $longitudDestino = Session::get('longitud');

            if(is_null($latitudDestino) or is_null($longitudDestino) ) {
                return false;
            }

            $coordenada_destino = new COORDENADA_DESTINO();
            $coordenada_destino->latitud = $latitudDestino;
            $coordenada_destino->longitude = $longitudDestino;
            $coordenada_destino->active = true;
            $coordenada_destino->codigo_arcliente = $codigo_cliente;

            $coordenada_destino->save();

            return true;
        } catch(\Exception $e) {
            Log::error("\nArchivo: " . $e->getFile() .
                "\nLínea: " . $e->getLine() .
                "\nBase : Remota" .
                "\nFuncion: insertCoordenadaDestino" .
                "\nMensaje: " . $e->getMessage());
            return false;
        }
    }

    /**
     * Haversine formula with php:
     * https://stackoverflow.com/a/14751773
     *
     * Calculates the great-circle distance between two points, with
     * the Haversine formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    public function haversineGreatCircleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000.0)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }
}
