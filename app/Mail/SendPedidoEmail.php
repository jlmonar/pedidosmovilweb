<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPedidoEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $path_to_file;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($path_to_file)
    {
        $this->path_to_file = $path_to_file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.pedido')
            ->subject('Pedido realizado')
            ->attach($this->path_to_file, [
                'as' => 'Pedido.pdf',
                'mime' => 'application/pdf',
            ]);
    }
}
