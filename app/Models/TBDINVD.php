<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TBDINVD extends Model
{
    protected $table = 'TBDINVD';

    protected $primaryKey = 'DINVD_DINV';

    public $timestamps = false;
}
