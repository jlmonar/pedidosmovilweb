<?php
/**
 * Created by PhpStorm.
 * User: Jose Luis
 * Date: 16/07/20
 * Time: 17:19
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PERMISO extends Model
{
    protected $table = 'PERMISO';

    protected $primaryKey = 'idPERMISO';

    public $timestamps = false;
}