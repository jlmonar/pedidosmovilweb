<?php

namespace App\Models;

use App\Transformers\TbbimTipoclTransformer;
use Illuminate\Database\Eloquent\Model;

class TBBIM_TIPOCL extends Model
{
    protected $table = 'TBBIM_TIPOCL';

    protected $primaryKey = 'TIP_CODIGO';

    public $timestamps = false;
}
