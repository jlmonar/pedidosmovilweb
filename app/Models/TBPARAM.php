<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TBPARAM extends Model
{
    protected $table = 'TBPARAM';

    protected $primaryKey = 'PARAM_CO';

    public $timestamps = false;
}
