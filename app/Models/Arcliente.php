<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ARCLIENTE extends Authenticatable
{
    use Notifiable;

    protected $table = 'ARCLIENTE';

    protected $primaryKey = 'CODIGO';

    public $timestamps = false;

    public $incrementing = false;
}
