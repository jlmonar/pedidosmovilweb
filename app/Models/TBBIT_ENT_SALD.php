<?php
/**
 * Created by PhpStorm.
 * User: Jose Luis
 * Date: 15/07/20
 * Time: 19:43
 */

namespace App\Models;


use App\Overrides\ModelCompositeKey;

class TBBIT_ENT_SALD extends ModelCompositeKey
{
    protected $table = 'TBBIT_ENT_SALD';

    protected $primaryKey =  array('EMPRESA', 'TIPO_ID', 'CODIGO');

    public $timestamps = false;
}