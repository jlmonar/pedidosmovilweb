<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TBCINV extends Model
{
    protected $table = 'TBCINV';

    protected $primaryKey = 'CINV_SEC';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function tbdinvs() {
        return $this->hasMany('App\Models\TBDINV', 'DINV_CTINV');
    }

    public function getCinvFecingAttribute() {
        $fecha = date("d/m/Y", strtotime($this->attributes['CINV_FECING']));

        return $fecha;
    }
}
