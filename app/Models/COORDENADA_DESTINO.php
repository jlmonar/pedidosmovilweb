<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class COORDENADA_DESTINO extends Model
{
    protected $table = 'COORDENADA_DESTINO';

    protected $primaryKey = 'id';

    public $timestamps = false;
}
